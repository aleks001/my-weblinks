---
title: "anything about text processing"
description: "pandoc, markdown, asciidoc, ..."
---

## pandoc

* [Building Publishing Workflows with Pandoc and Git](https://publishing.sfu.ca/2013/11/building-publishing-workflows-with-pandoc-and-git/)
* [Meine Tools fürs Ebook-Schreiben](https://www.wolfgang-mechsner.de/2017/01/01/meine-tools-fuers-ebook-schreiben/)