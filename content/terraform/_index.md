---
title: "hashicorp Projects"
description: "terraform, packer, ..."
---

## terraform

* [A Comprehensive Guide to Terraform](https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca)
* [terraform.py is a dynamic Ansible inventory script to connect to systems by reading Terraform's .tfstate](https://github.com/mantl/terraform.py)